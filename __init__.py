import json
import sqlite3
import tempfile

from typing import Dict, List

from mycroft import MycroftSkill, intent_handler
from mycroft.messagebus import Message
from mycroft.util.log import LOG

from fuzzywuzzy import fuzz, process


class BookmarkNotFoundError(Exception):
    pass


class FirefoxNotFoundError(Exception):
    pass


class Bookmarker:

    CONFIDENCE_THRESHOLD = 75

    def __init__(self, bookmarks_path: str):
        """
        For now, we're making the assumption that the bookmarks don't change
        after start up.  We also only support Firefox right at the moment.
        """

        self.logger = LOG.create_logger(self.__class__.__name__)

        self.bookmarks_path = bookmarks_path

        self.db = {}
        self.db.update(self._get_bookmarks())

    def search(self, phrase: str) -> List[Dict[str, str]]:

        if not self.bookmarks_path:
            raise FirefoxNotFoundError(
                "Bookmark search is unavailable due to the absence of a "
                "bookmarks_path.  Visit home.mycroft.ai to configure this "
                "skill with your key."
            )

        self.logger.info("Searching for %s in bookmarks", phrase)
        ranked = process.extract(
            phrase, self.db.keys(), limit=23, scorer=fuzz.token_set_ratio
        )

        r = []

        for title, score in ranked:
            self.logger.info("Score: %s (%s)", score, title)
            if score > self.CONFIDENCE_THRESHOLD:
                r.append({"title": title, "url": self.db[title]})
            else:
                break  # Sorting the ranked means we can exit early

        if not r:
            raise BookmarkNotFoundError("No reasonable match could be found")

        return r

    def _get_bookmarks(self) -> Dict[str, str]:
        """
        Firefox establishes an exclusive lock on the Sqlite database, so you
        can't even SELECT from it.  Instead, we just copy the whole db into a
        temporary file, and query *that* file, then delete it.
        """

        if not self.bookmarks_path:
            return {}

        query = """
            SELECT
                i.title,
                u.url
            FROM
                urls AS u
            INNER JOIN
                items i ON i.urlID = u.id
            WHERE
                i.title IS NOT NULL AND
                i.title <> '';
        """
        with tempfile.NamedTemporaryFile() as tmp:
            with open(self.bookmarks_path, "rb") as source:
                tmp.write(source.read())
            with sqlite3.connect(tmp.name) as c:
                r = dict(c.cursor().execute(query).fetchall())

        return r


class BookmarksSkill(MycroftSkill):
    """
    Messages emitted:
      * skill.majel.browser.open-selector
      * skill.majel.browser.open
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        bookmarks_path = self.settings.get("bookmarks_path")

        self.bookmarker = None
        if bookmarks_path:
            self.bookmarker = Bookmarker(bookmarks_path=bookmarks_path)
        else:
            self.log.warning(
                "It looks like the bookmarks haven't been configured yet.  "
                "You'll need to head over to https://home.mycroft.ai/ to "
                "configure this skill with the path to your Firefox bookmarks."
            )

    @intent_handler("bookmarks.intent")
    def handle_bookmarks(self, message):

        if not self.bookmarker:
            self.log.warning("Bookmarks haven't been configured.")
            return

        try:
            urls = self.bookmarker.search(message.data.get("query"))
        except BookmarkNotFoundError:
            self.speak_dialog("not-found")
            return

        target = "skill.majel.browser.open-selector"
        payload = {"urls": json.dumps(urls, separators=(",", ":"))}
        if len(urls) == 1:
            target = "skill.majel.browser.open"
            payload = {"url":  urls[0]["url"]}

        self.log.info("Emitting %s → %s", target, payload)
        self.bus.emit(Message(target, payload))

        self.speak_dialog("found")


def create_skill():
    return BookmarksSkill()

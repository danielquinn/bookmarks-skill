# bookmarks-skill

A Mycroft skill that leverages [Majel](https://pypi.org/project/majel/) to
search your Firefox bookmarks.


## Configuration

For the moment, reading your bookmarks is a convoluted process of actually
reading the `.sqlite` bookmarks file right out of your Firefox home directory.
Ideally, I'd like to clean this up a bit in the future and actually talk to
their [storage API](https://mozilla-services.readthedocs.io/en/latest/storage/index.html),
but that's *a lot* more complicated, so this will have to do for now.

To configure this skill, you need only head over to [home.mycroft.ai](https://home.mycroft.ai/)
and find this skill under the list of configurable skills.  The value you put
into the `Firefox` form field should be the path to your Firefox bookmarks
file, so for example it might be something like this:

    /home/username/.mozilla/firefox/abcdefgh.default/weave/bookmarks.sqlite


## Use

There are two modes for this skill: single results and multiple.  When you say
something like:

> *"Hey Mycroft, search my bookmarks for chicken"*

Mycroft will do a fuzzy match search for "chicken" against all of your
bookmarks.  If it finds one recipe (you need more recipes!) then your browser
will simply go to that link.  If however it finds multiple chicken recipes
(that's better!) then it'll render-out a page with tap-friendly buttons
representing each bookmark.  Tap one and it'll take you there.


## Caveats

As Firefox uses an exclusive lock its Sqlite bookmarks database, we can't read
the file directly.  Instead, we make a local copy at start time and read that.
This means that if you're actively updating your bookmarks, those updates won't
be reflected in your bookmark searches until you restart Mycroft.  I'm open to
better ideas on this one.
